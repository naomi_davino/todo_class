<?php

use Illuminate\Database\Seeder;

class TodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            [
                'title' => 'By milk',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Preper for the test',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Read a book',
                'user_id' => 2,
                'created_at' => date('Y-m-d G:i:s')
            ]
        ]);
    }
}
