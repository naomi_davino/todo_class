<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                // [
                //     'name' => 'Noam',
                //     'email' => 'noam@gmail.com',
                //     'password' => Hash::make(12345),
                //     'created_at' => date('Y-m-d G:i:s'),
                //     'role' => 'manager',
                //  ],

                [
                'name' => 'nir',
                'email' => 'nir@gmail.com',
                'password' => Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'employee',
                ],
                
                [
                    'name' => 'gal',
                    'email' => 'gal@gmail.com',
                    'password' => Hash::make('1234'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'employee',  
                ],
                


        ]);
    }
}
