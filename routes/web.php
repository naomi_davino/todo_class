<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "Hello World";
});

Route::get('/student/{id?}', function ($id = 'no student provided') {
    return "Hello student ".$id;
})->name('students');

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id' => $id]);
})->name('comments');

// תרגיל 1
Route::get('/customers/{id?}', function ($id = 'No customer was provided') {
   if ($id=='No customer was provided') {
        return view('nocustomers',['id' => $id]);   
   } 
   else {
        return view('customers',['id' => $id]); 
   } 
})->name('customers');

//הרצאה 4
Route::resource('todos', 'TodoController')->middleware('auth'); 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

